const path = require('path');

var publicPath;

if (process.env.NODE_ENV == "development") {
    publicPath = "xxxxx/static/";
} else {
    publicPath = "xxxxxxhttps://domma.gitlab.io/sico/";
}

module.exports = {
  mode: "production",
  entry: {
    admin: './frontend/src/bundles/admin.js',
    search: './frontend/src/bundles/search.js'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/i,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/,
        use: 'file-loader'
      }
    ],
  },
  resolve: {
    modules: [path.resolve(__dirname, 'src'), 'node_modules'],
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  output: {
    filename: 'bundle-[name]-[contenthash].js',
    path: path.resolve(__dirname, '../sico/web/bundles'),
    publicPath: publicPath
  },
};
