import { Controller } from "stimulus"

var Turbolinks = require("turbolinks");

export default class extends Controller {
    _exec(url, method, body) {
        console.log("turbo-exec:" + url + ", " + method);
        if (body && (typeof body != "string")) {
            body = JSON.stringify(body);
        }

        fetch(url, {
            method: method,
            cache: "no-cache",
            body: body
        }).then((resp) => {
            console.log(resp);
            return resp.text();
        }).then((text) => {
            Function(text).apply(this);
        });
    }

    exec_get(url) {
        this._exec(url, "GET");
    }
    exec_put(url, body) {
        this._exec(url, "PUT", body);
    }
    exec_delete(url, body) {
        this._exec(url, "DELETE", body);
    }
    exec_post(url, body) {
        this._exec(url, "POST", body);
    }

    display_error(msg) {
        let error = document.getElementById("error");
        error.classList.add("notification");
        error.classList.add("is-danger");
        error.innerHTML = "<pre>" +  msg + "</pre>";
        error.scrollIntoView(true);
    }
}
