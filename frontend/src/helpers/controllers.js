import { Application } from "stimulus";
import { definitionsFromContext } from "stimulus/webpack-helpers";

export const init = function () {
    var Turbolinks = require("turbolinks");
    Turbolinks.start();

    const application = Application.start();
    const context = require.context("../controllers", true, /\.js$/);
    application.load(definitionsFromContext(context));
}