import "awesomplete";
import "awesomplete/awesomplete.css";
import "../css/awesomplete-theme.css";
/**
 * @typedef {import('../typings/autocomplete').AutcompleteDataInputEntry} AutcompleteDataInputEntry
 */

/**
 * @typedef {import('../typings/autocomplete').AutocompleteEntry} AutocompleteEntry
 */

/**
 * @typedef {import('../typings/autocomplete').InitReturn} AutocompleteInitReturn
 */

/**
 * Enhances the input data to work well will Awesomplete.
 * @param {Array<AutcompleteDataInputEntry>} data
 * @returns {Array<AutocompleteEntry>}
 */
export const createAwesomepleteList = (data) =>
  data.map((entry) => ({ ...entry, value: entry.id }));

/**
 *
 * @param {Element} inputField
 * @param {Array<AutocompleteEntry>} list
 * @returns {AutocompleteInitReturn}
 */
export const initAwesomplete = (inputField, list) => {
  const style = document.createElement("style");
  document.head.appendChild(style);

  const documents = document.getElementById("documents");
  const enableFilterMode = () => documents.classList.add("filtered");
  const disableFilterMode = () => documents.classList.remove("filtered");
  inputField.addEventListener("awesomplete-open", () => {
    //Create CSS
    documents.classList.add("awesomplete-active");
  });
  inputField.addEventListener("awesomplete-close", () => {
    //Remove CSS
    documents.classList.remove("awesomplete-active");
    if (inputField.value === "") {
      disableFilterMode();
    }
  });

  inputField.addEventListener("awesomplete-select", (e) => {
    const selectedId = e.text.value;

    enableFilterMode();

    // Remove all previous rules
    for (let i = 0; i < style.sheet.cssRules.length; i++) {
      style.sheet.deleteRule(i);
    }

    style.sheet.insertRule(".documents.filtered li {display: none;}");
    style.sheet.insertRule(
      `.documents.filtered li[id="doc_${selectedId}"] {display: block;}`
    );
  });

  inputField.addEventListener("change", (e) => {
    if (e.target.value === "") {
      disableFilterMode();
    }
  });

  inputField.addEventListener("blur", (e) => {
    if (e.target.value === "") {
      disableFilterMode();
    }
  });

  // Return array of things: 1. autocomplete instance, 2. related styles
  return [
    new Awesomplete(inputField, {
      list,
      replace: function (item) {
        this.input.value = item.label;
      },
    }),
    style,
  ];
};

export const init = () => {};
