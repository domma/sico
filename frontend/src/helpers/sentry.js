import * as Sentry from '@sentry/browser';

export const init = function () {
    Sentry.init({
        dsn:
            "https://c09e5c044ec84561ab1b99caaea29fdc@o362714.ingest.sentry.io/5227234",
    });
}
