export type AutcompleteDataInputEntry = {
  id: string;
  label: string;
};

export type AutocompleteEntry = AutcompleteDataInputEntry & {
  value: string;
};

export type InitReturn = [Awesomplete, HTMLStyleElement];
