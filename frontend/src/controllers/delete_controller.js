import { Controller } from "stimulus"
import { Draggable } from '@shopify/draggable';

import TurboController from "../turbocontroller.js"

export default class extends TurboController {
    connect() {
        this.modal = document.getElementById("yesno");

        let controller = this;

        this.modal.querySelector(".yes").addEventListener("click", 
            function() {
                if (controller.url) {
                    controller.exec_delete(controller.url, {})
                }
            }
        );

        this.modal.querySelector(".no").addEventListener("click",
            function() {
                controller.url = null;
                controller.modal.classList.remove("is-active");
            }
        );
    }

    ask(e) {
        this.url = e.currentTarget.getAttribute("data-item-url");
        if (this.url) {
            this.modal.classList.add("is-active");
        }
    }
}
