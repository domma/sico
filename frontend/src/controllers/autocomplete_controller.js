import { Controller } from "stimulus";
import {
  createAwesomepleteList,
  initAwesomplete,
} from "../helpers/autocomplete";
export default class extends Controller {
  connect() {
    const filterInput = document.getElementById("filter");

    const [autocompleteInstance, style] = initAwesomplete(
      filterInput,
      createAwesomepleteList(window.filter_data)
    );

    this.style = style;
    this.autocompleteInstance = autocompleteInstance;
  }

  disconnect() {
    // Destroy autocomplete instance
    this.autocompleteInstance.destroy();

    // Remove generated CSS stylesheet
    document.head.removeChild(this.style);
  }
}
