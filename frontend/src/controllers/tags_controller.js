import { Controller } from "stimulus"
import TurboController from "../turbocontroller.js"

export default class extends TurboController {
    show_only(e) {

        let buttons = this.element.getElementsByClassName("button");
        for (let but of buttons) {
            but.classList.remove("is-light");
            but.classList.remove("has-text-weight-bold");
        };
        e.target.classList.add("is-light");
        e.target.classList.add("has-text-weight-bold");

        let ids = JSON.parse(e.target.getAttribute("data-docs-ids"));
        let docs = document.querySelectorAll(".documents > li");

        for (let d of docs) {
            let id = parseInt(d.id.split("_")[1]);
            d.hidden = !ids.includes(id);
        };
    }
}
