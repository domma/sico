import { Controller } from "stimulus"

import TurboController from "../turbocontroller.js"

export default class extends TurboController {
    connect() {
      this.editor = ace.edit("editor");
      if (this.editor) {
          this.editor.setTheme("ace/theme/solarized_light");
          this.editor.session.setMode("ace/mode/yaml");
          document.getElementById('editor').style.fontSize='1.2em';
      }
  }

    create() {
        let target_url = this.element.getAttribute("data-target-url");
        this.exec_post(target_url, this.editor.getValue());
    }

    save() {
        let target_url = this.element.getAttribute("data-target-url");
        this.exec_put(target_url, this.editor.getValue());
    }
}
