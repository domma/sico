import { Controller } from "stimulus"
import { Draggable } from '@shopify/draggable';

import TurboController from "../turbocontroller.js"

export default class extends TurboController {
    connect() {
        this.move_url = this.element.getAttribute("data-url");
        this.drag = new Draggable(this.element, { draggable: '.tag' });
        this.ctrlKey = false;

        this.drag.on("drag:start", (e) => {
            let group_id = parseInt(this.drag.source.getAttribute("data-group-id"));
            if (group_id==0) {
                e.cancel();
                return;
            }
            this.ctrlKey = e.originalEvent.ctrlKey;
        });
        this.drag.on("drag:over", (e) => {
            this.ctrlKey = e.originalEvent.ctrlKey;
            if (e.over != e.source) {
                e.over.classList.add("is-success");
            }
        });
        this.drag.on("drag:out", (e) => {
            e.over.classList.remove("is-success");
        });
        this.drag.on("drag:stop", (e) => {

            let target = this.drag.currentOver;

            target.classList.remove("is-success");

            let group_id = parseInt(this.drag.source.getAttribute("data-group-id"));
            let old_parent = parseInt(this.drag.source.getAttribute("data-parent-id"));
            let new_parent = parseInt(target.getAttribute("data-group-id"));

            let action;

            if (old_parent != new_parent) {
                if (this.ctrlKey) {
                    action =  {
                        add: group_id,
                        source: old_parent,
                        target: new_parent
                    };
                } else {
                    action =  {
                        move: group_id,
                        source: old_parent,
                        target: new_parent
                    };
                }

                this.exec_post(this.move_url, action);
            }
            this.ctrlKey = false;
        });
    }
}
