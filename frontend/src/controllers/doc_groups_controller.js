import { Controller } from "stimulus"
import { Draggable } from '@shopify/draggable';

import TurboController from "../turbocontroller.js"

export default class extends TurboController {
    connect() {
        console.log("doc-groups connected");
        this.url = this.element.getAttribute("data-url");
    }

    toggle(e) {
        console.log("toggle:", e);
        let group_id = parseInt(e.target.getAttribute("data-group-id"));
        let active = e.target.getAttribute("data-is-active");

        if (active) {
            this.exec_delete(
                this.url, {group_id: group_id}
            );
        } else {
            this.exec_post(
                this.url, {group_id: group_id}
            );
        }
    }
}
