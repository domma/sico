import { Controller } from "stimulus"

import TurboController from "../turbocontroller.js"

export default class extends TurboController {
    connect() {
        this.toggle_class = this.element.getAttribute("data-toggle-class");
  }

    toggle(e) {
        let target_class = e.currentTarget.getAttribute("data-target");
        let classList = this.element.getElementsByClassName(target_class)[0].classList;
        if (classList.contains(this.toggle_class)) {
            classList.remove(this.toggle_class);
        } else {
            classList.add(this.toggle_class);
        }
    }
}
