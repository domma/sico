// This is going to be the bundle that is loaded on the search page.
import { init as initControllers } from "helpers/controllers";
import { init as initSentry } from "helpers/sentry";
import { init as initAutocomplete } from "helpers/autocomplete";

import "bulma/css/bulma.min.css";
import "../css/search.css";

initSentry();
initControllers();
initAutocomplete();
