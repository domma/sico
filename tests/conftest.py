"""
PyTest Fixtures.
"""
from contextlib import contextmanager

import pytest

from sqlalchemy import create_engine
from sqlalchemy.pool import NullPool
import falcon.testing

from sico.db.api import Session, init
import sico.web


# @pytest.fixture(scope="function")
# def session(postgresql_db):
#    api.init(postgresql_db.engine)
#    yield postgresql_db.session


class SessionHandler:
    def __init__(self, engine):
        self.engine = engine

    @contextmanager
    def new(self):
        session = Session(bind=self.engine)
        try:
            yield session
        finally:
            session.commit()
            session.close()


@pytest.fixture(scope="function")
def engine(postgresql):
    dsn = postgresql.get_dsn_parameters()
    engine = create_engine(
        f"postgresql://{dsn['user']}@{dsn['host']}:{dsn['port']}/{dsn['dbname']}",
        poolclass=NullPool,
    )
    init(engine)
    yield engine
    engine.dispose()


@pytest.fixture(scope="function")
def session(engine):
    yield SessionHandler(engine)


@pytest.fixture(scope="function")
def web(engine):
    yield falcon.testing.TestClient(sico.web.create_app(engine, True, test_mode=True))
