import sico.db.schema as s
import sico.db.group_filters as gf


def test_api(session):
    with session.new() as api:
        api.add(group1 := s.Group.from_yaml("name: 'group1'"))
        api.add(group2 := s.Group.from_yaml("name: 'group2'\nhidden: True"))

        [visible] = api.query(s.Group).filter(gf.visible).all()

        assert visible.id == group1.id
