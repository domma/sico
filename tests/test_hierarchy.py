import pytest

import sico.db.schema as s


def test_api(session):
    with session.new() as api:
        api.add(s.User(login="admin", password="admin"))
    with session.new() as api:
        assert api.get_user("admin", "admin")


def test_add_parent(session):
    with session.new() as api:
        api.add(group1 := s.Group.from_yaml("name: 'group1'"))
        api.add(group2 := s.Group.from_yaml("name: 'group2'"))
        api.commit()
        group2.add_parent(group1)

        id1 = group1.id
        id2 = group2.id

    with session.new() as api:
        g = api.by_id(s.Group, id2)
        assert g.parents == [id1]


def test_add_requires_group():
    group1 = s.Group.from_yaml("name: 'group1'")
    with pytest.raises(ValueError):
        group1.add_parent(1)


def test_no_add_to_self(session):
    with session.new() as api:
        api.add(group1 := s.Group.from_yaml("name: 'group1'"))
        api.commit()

        with pytest.raises(Exception):
            group1.add_parent(group1)


def test_no_circles(session):
    with session.new() as api:
        api.add(group1 := s.Group.from_yaml("name: 'group1'"))
        api.add(group2 := s.Group.from_yaml("name: 'group2'"))
        api.add(group3 := s.Group.from_yaml("name: 'group3'"))
        api.commit()

        group1.add_parent(group2)
        group2.add_parent(group3)

        with pytest.raises(Exception):
            group3.add_parent(group1.id)


def test_names(session):
    with session.new() as api:
        api.add(group1 := s.Group.from_yaml("name: 'group1'"))
        api.add(group2 := s.Group.from_yaml("name: 'group2'"))
        api.commit()

    names = api.get_names(s.Group)
    assert len(names) == 2


def test_result(session):
    with session.new() as api:
        api.add(group1 := s.Group.from_yaml("name: 'group1'"))
        api.commit()
        id = group1.id

    with session.new() as api:
        group1 = api.by_id(s.Group, id)
        assert group1.data["name"] == "group1"


def test_tag_tree(session):
    with session.new() as api:
        api.add(group1 := s.Group.from_yaml("name: 'group1'"))
        api.add(group2 := s.Group.from_yaml("name: 'group2'"))
        api.add(group3 := s.Group.from_yaml("name: 'group3'"))
        api.add(group4 := s.Group.from_yaml("name: 'group4'"))
        api.add(group5 := s.Group.from_yaml("name: 'group5'"))
        api.commit()

        group3.add_parent(group2)
        group5.add_parent(group4)
        group2.add_parent(group1)
        group4.add_parent(group1)
        api.commit()

        tree = api.get_tag_tree()

        assert len(tree) == 1
        assert tree[0]["id"] == group1.id
        assert len(tree[0]["children"]) == 2
