def test_root(web):
    result = web.simulate_get("/search/")
    assert result.status_code == 200


def test_groups(web):
    result = web.simulate_get("/groups")
    assert result.status_code == 200


def test_docs(web):
    result = web.simulate_get("/docs")
    assert result.status_code == 200
