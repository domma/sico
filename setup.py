from setuptools import setup, find_packages

VERSION = "0.0.1"

f = open("README.md", "r")
LONG_DESCRIPTION = f.read()
f.close()

setup(
    name="sico",
    version=VERSION,
    description="MyApp Does Amazing Things!",
    long_description=LONG_DESCRIPTION,
    long_description_content_type="text/markdown",
    author="Achim Domma",
    author_email="achim@domma.de",
    url="url",
    license="GPL 3.0",
    packages=find_packages(exclude=["ez_setup", "tests*"]),
    package_data={"sico": ["templates/*"]},
    include_package_data=True,
    entry_points="""
        [console_scripts]
        sico = sico.cmds.main:main
    """,
)
