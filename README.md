# SImple COntent

[[_TOC_]]

## Installation

Sico is prepared to be deployed on Heroku. Usually you would install
a Python package for development via

```bash
pip install -e .
```

To be consistent between local development and Heroku, you have to
install sico via

```bash
$ pip install -r requirements.txt
```

The `-e .` was added to requirements.txt, so the local package is
automatically installed with the above command and the sico command
line tool becomes available. The CLI is based on https://click.palletsprojects.com/en/7.x/ so you get details by calling
"sico --help".

@Andi: The frontend is using npm and is build via `npm build`. Docs have to be extended, but assume you will manage to
get this working ;-) 


## Local development

Local development needs a Postgres database. Assuming you have local
Postgres server running and connect as database admin, you could create
the database like this:

```sql
create database sico;
create user sico with password 'your_password';
grant all on database sico to sico;
```

Sico expects the database connection in the environment variable `DATABASE_URL`.
How to set it, depends on your shell. It should look like this:

```bash
export DATABASE_URL="postgres://sico:your_password@localhost:5432/sico"
```

Now you can initialize the database:

```bash
sico db init
```

You can now start using the empty database. But if you got a backup from
an existing database, you can import it like this:

```bash
sico db restore data-file.jsonl
```

Databases can exported into jsonl format. Import from jsonl makes the
import more schema agnostic and should make schema changes in the future
a bit simpler. A backup can be created via

```bash
sico db backup data-file.jsonl
```

Now its time to start your local server. Make sure that the `DATABASE_URL` is
still set. You also have to set the `SECRET` environment variable to a
random string which is used for encrypting cookies. Then call

```bash
sico web serve --dev
```

The `--dev` flag will automatically call `npm build` in the frontend folder
and the resulting JS bundle is copied to the static folder of the web app
code.

For local development I use this script (Fish shell syntax) to start
my local server:

```bash
set -x DATABASE_URL postgres://sico:some_password@localhost:5432/sico
set -x SECRET blablub
watchexec -c -w frontend -w sico -i sico/web/static -e js,py,jinja2 -r -- sico web serve --dev
```


## Architecture / tooling

To be extended later, but some hints to get started:

* Backend is pure Python and Postgres, making use of Postgres specific
  functionality like ARRAY and JSONB columns.
* The command line interface is build with https://click.palletsprojects.com/en/7.x/
* Database access is implemented via the "core" of https://www.sqlalchemy.org/. No OO mapper is used.
* The web app is implemented using https://falconframework.org/ and run via https://github.com/jonashaag/bjoern
* None of the current fancy frontend libraries is used. The HTML is rendered
  on the server side using https://jinja.palletsprojects.com/en/2.11.x/. The
  frontend uses https://stimulusjs.org/ and https://github.com/turbolinks/turbolinks,
  which is a VERY nice tech stack if you don't like Javascript that much. ;-)
* Layout is using https://bulma.io/ and we have full Fontawesome license.
