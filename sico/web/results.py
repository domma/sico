import json

import falcon

from sico.web import jinja


class Result:
    def create_response(self, resp):
        raise NotImplementedError("Result classes must implement create_response")


class Jinja(Result):
    def __init__(self, template, data={}, tl=None):
        self.template = template
        self.data = data
        self.tl = tl

    def create_response(self, resp):
        resp.body = jinja.render(self.template, self.data)
        resp.content_type = falcon.MEDIA_HTML
        if self.tl:
            resp.append_header("Turbolinks-Location", self.tl)


class Error(Result):
    def __init__(self, message):
        self.message = message

    def create_response(self, resp):
        msg = json.dumps(self.message)
        resp.body = f"this.display_error({msg});"
        resp.content_type = falcon.MEDIA_TEXT


class Json(Result):
    def __init__(self, data):
        self.data = data

    def create_response(self, resp):
        resp.body = json.dumps(self.data)
        resp.content_type = falcon.MEDIA_JSON


class Redirect(Result):
    def __init__(self, target):
        self.target = target

    def create_response(self, resp):
        action = json.dumps({"action": "replace"})
        resp.body = (
            f'Turbolinks.clearCache(); Turbolinks.visit("{self.target}", {action});'
        )
        resp.content_type = falcon.MEDIA_TEXT
