class Position:
    @classmethod
    def decode_groups(cls, encoded):
        try:
            return [int(i) for i in encoded.split(",")]
        except ValueError:
            return []

    @classmethod
    def encode_groups(cls, groups):
        return ",".join([str(i) for i in groups])

    def __init__(self, encoded=None, groups=None):
        if encoded and groups:
            raise RuntimeError("You must pass only encoded or groups as argument.")
        if not (encoded or groups):
            self.ids = []
        elif encoded:
            self.ids = self.decode_groups(encoded)
        else:
            self.ids = groups

    def up(self):
        return Position(groups=self.ids[:-1])

    def down(self, group_id):
        return Position(groups=self.ids + [group_id])

    @property
    def at_top(self):
        return not self.ids

    def __str__(self):
        return self.encode_groups(self.ids)

    @property
    def leaf(self):
        if self.ids:
            return self.ids[-1]
