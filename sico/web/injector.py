from inspect import signature
from functools import wraps
import json


def create_custom_injector(fkt, available_injectors):
    injectors = {}
    sig = signature(fkt)
    valid_names = []

    for name in sig.parameters:
        if name in available_injectors:
            injectors[name] = available_injectors[name]
        else:
            valid_names.append(name)

    def injector(self, req, resp, kwargs):
        return (
            {
                key: get_val(self, req, resp, kwargs)
                for key, get_val in injectors.items()
            },
            {k: v for k, v in kwargs.items() if k in valid_names},
        )

    return injector


def create_injection_decorator(available_injectors):
    def decorator(fkt):
        injector = create_custom_injector(fkt, available_injectors)

        @wraps(fkt)
        def _fkt(self, req, resp, **kwargs):
            values, kwargs = injector(self, req, resp, kwargs)
            return fkt(self, **kwargs, **values)

        return _fkt

    return decorator


def inject_entity(self, req, resp, kwargs):
    entity_id = kwargs.get("entity_id")
    if not entity_id:
        raise RuntimeError("Could not get 'entity_id' while trying to inject entity.")
    return (
        req.context.session.query(self.Entity).filter(self.Entity.id == entity_id).one()
    )


def inject_req(self, req, resp, kwargs):
    return req


def inject_resp(self, req, resp, kwargs):
    return resp


def inject_json_body(self, req, resp, kwargs):
    return json.load(req.stream)


def inject_txt_body(self, req, resp, kwargs):
    return req.stream.read().decode("utf-8")


def inject_session(self, req, resp, kwargs):
    return req.context.session


default_injectors = {
    "entity": inject_entity,
    "req": inject_req,
    "resp": inject_resp,
    "json_body": inject_json_body,
    "txt_body": inject_txt_body,
    "session": inject_session,
    "api": inject_session,
}

# inject = create_injection_decorator(default_injectors)
