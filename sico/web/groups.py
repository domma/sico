import logging


from sico.web.entity import Entity, Entities
from sico.web.endpoint import endpoint
from sico.web.results import Jinja, Redirect
import sico.db.schema as s


LOG = logging.getLogger(__name__)


class Groups(Entities):
    entity_name = "group"
    Entity = s.Group

    @endpoint()
    def on_get(self, api):
        return Jinja("groups/overview.jinja2", {"tree": api.get_tag_tree()})

    @endpoint()
    def on_post_tree(self, json_body):
        LOG.debug(f"Move tag: {json_body}")
        if "move" in json_body or "add" in json_body:
            target = json_body["target"]
            source = json_body["source"]

        if "add" in json_body:
            add = json_body["add"]
            if target:
                child = api.by_id(self.Entity, add)
                api.by_id(self.Entity, target).add_child(child)
            return Redirect("/groups")

        if "move" in json_body:
            move = json_body["move"]
            child = api.by_id(self.Entity, move)
            api.by_id(self.Entity, source).remove_child(child)
            if target:
                api.by_id(self.Entity, target).add_child(child)
            return Redirect("/groups")


class Group(Entity):
    entity_name = "group"
    Entity = s.Group


def setup(app):
    group = Group()
    app.add_route("/group/{entity_id:int}", group)
    app.add_route("/group/{entity_id:int}/edit", group, suffix="edit")
    #    app.add_route("/group/{entity_id:int}/parents", group, suffix="parents")
    app.add_route("/group/{entity_id:int}/children", group, suffix="children")

    groups = Groups()
    app.add_route("/groups", groups)
    app.add_route("/groups/new", groups, suffix="new")
    app.add_route("/groups/tree", groups, suffix="tree")
