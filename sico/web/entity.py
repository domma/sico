import logging

from sico.web.results import Jinja, Redirect, Error
from sico.web.endpoint import endpoint

LOG = logging.getLogger(__name__)


class Entities:
    @endpoint()
    def on_post(self, session, txt_body):
        try:
            session.add(self.Entity.from_yaml(txt_body))
        except (Exception) as e:
            return Error(str(e))
        return Redirect(f"/{self.entity_name}s")

    @endpoint()
    def on_get(self, api):
        return Jinja(
            f"{self.entity_name}s/overview.jinja2",
            {"items": api.get_names(self.Entity), "entity_name": self.entity_name,},
        )

    @endpoint()
    def on_get_new(self):
        return Jinja("new_item.jinja2", {"entity_type": self.entity_name})


class Entity:
    @endpoint()
    def on_get_edit(self, entity, entity_id):
        return Jinja(
            "edit_item.jinja2",
            {"entity": entity, "target_url": f"/{self.entity_name}/{entity.id}",},
            tl=f"/{self.entity_name}/{entity.id}/edit",
        )

    @endpoint()
    def on_put(self, entity, entity_id, txt_body):
        try:
            entity.update_source(txt_body)
        except (Exception) as e:
            return Error(str(e))
        return Redirect(f"/{self.entity_name}s")

    @endpoint()
    def on_delete(self, session, entity):
        session.delete(entity)
        return Redirect(f"/{self.entity_name}s")

    #    @endpoint()
    #    def on_post_parents(self, entity, json_body):
    #        tag_id = json_body["tag_id"]
    #        entity.add_parent(tag_id)

    @endpoint()
    def on_post_children(self, api, entity, json_body):
        tag_id = json_body["tag_id"]
        child = api.by_id(self.Entity, tag_id)
        if child:
            entity.add_child(child)

    #    @endpoint()
    #    def on_delete_parents(self, entity, json_body):
    #        tag_id = json_body["tag_id"]
    #        entity.remove_parent(tag_id)

    @endpoint()
    def on_delete_children(self, entity, json_body):
        tag_id = json_body["tag_id"]
        child = api.by_id(self.Entity, tag_id)
        if child:
            entity.remove_child(child)
