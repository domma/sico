import sys
import itertools
import collections

import falcon
from sqlalchemy import func, Boolean

from sico.open_status import set_open_status
from sico.web.position import Position
from sico.web.endpoint import endpoint
from sico.web.results import Jinja
import sico.db.group_filters as gf

from sico.db.schema import Doc, Group


def get_selected(level, selected_id):
    if selected_id:
        try:
            return next(g for g in level if g.id == selected_id)
        except StopIteration:
            #   Looks like the selected id is not valid for this
            #   level. This should never happen, but somebody might
            #   enter crappy data or the data has changed. We ignore
            #   this and go forward with the default behavior for
            #   levels without explicit selection.
            pass

    #   TODO: In a future version we probably mark the default for
    #   a given groups children. This would be the place to pick
    #   that one.

    #   No idea what to pick, so take the first one.
    return level[0]


class SearchStatus:
    def __init__(self, tree, selected_ids):
        if not tree:
            raise RuntimeError("Cannot create status for empty tree.")

        self.levels = []
        self.selected_groups = []

        level = tree
        while level:
            for group in level:
                ids = ",".join([str(g.id) for g in self.selected_groups + [group]])
                group.url = f"/search/{ids}"
            selected = get_selected(level, selected_ids[0] if selected_ids else None)
            self.selected_groups.append(selected)
            self.levels.append(level)
            level = selected.children
            selected_ids = selected_ids[1:]

        for g in self.selected_groups:
            g.active = True

    def get_filters(self):
        return sum((g.docs_filter() for g in self.selected_groups), start=[])

    @property
    def actions(self):
        for group in self.selected_groups[::-1]:
            if "actions" in group.data:
                return group.data["actions"]


class TagBag:
    def __init__(self, docs):
        tags = collections.defaultdict(list)
        for d in docs:
            for t in d.data.get("tags", []):
                tags[t].append(d.id)

        self.tags = list(tags.items())
        self.tags.sort()


class Search(object):
    def on_get_root(self, req, resp):
        raise falcon.HTTPSeeOther("/search/")

    @endpoint(public=True)
    def on_get(self, api, req, resp, status):
        position = Position(encoded=status)
        status = SearchStatus(api.get_tag_tree(), position.ids)

        def doc_rank(d):
            order = d.data.get("order", sys.maxsize)
            name = d.data.get("name")
            is_closed = False
            o = d.data.get("open_status")
            if o:
                is_closed = not o.is_open
            return (order, is_closed, name)

        filters = status.get_filters()
        #   For now we consider it a bug if there is no filter
        #   at all, so we don't return any document.
        if filters:
            docs = api.query(Doc).filter(*filters).all()
            set_open_status(docs)
            docs.sort(key=doc_rank)
        else:
            docs = []

        filter_data = [
            {"label": d.data.get("name"), "id": d.id} for d in docs if "name" in d.data
        ]

        """Handles GET requests"""
        return Jinja(
            "search/main.jinja2",
            {
                "status": status,
                "tags": TagBag(docs),
                "docs": docs,
                "filter_data": filter_data,
            },
        )
