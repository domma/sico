import os
import os.path
import shutil
import subprocess
import logging

from sico.web import jinja

LOG = logging.getLogger(__name__)
here = os.path.split(__file__)[0]
static_files = os.path.join(here, "static")
bundle_path = os.path.join(here, "bundles")


def bundle_base_name(fullname):
    parts = fullname.split("-")
    if not len(parts) == 3 or not parts[0] == "bundle":
        raise ValueError(f"Invalid bundle name: {fullname}")
    return parts[1]


class Static:
    def __init__(self):
        self.files = {}
        for name in os.listdir(static_files):
            with open(os.path.join(static_files, name), "rb") as f:
                self.files[name] = f.read()
        self.set_bundles()


    def on_get(self, req, resp, filename):
        if filename.startswith("bundle"):
            with open(os.path.join(bundle_path, filename), "rb") as f:
                resp.body = f.read()
        else:
            resp.body = self.files[filename]


    def set_bundles(self):
        jinja.set_bundles(
            {
                bundle_base_name(name): name
                for name in os.listdir(bundle_path)
                if name.endswith("js")
            }
        )


    def rebuild_bundles(self):
        if os.path.exists(bundle_path):
            shutil.rmtree(bundle_path)
        subprocess.run(["npm", "run", "build"], cwd=os.path.join(here,"../.."))
        self.set_bundles()
