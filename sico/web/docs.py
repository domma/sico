from sico.web.entity import Entities, Entity
from sico.web.endpoint import endpoint
from sico.web.results import Jinja
from sico.open_status import set_open_status
import sico.db.schema as s


class Docs(Entities):
    entity_name = "doc"
    Entity = s.Doc


class Doc(Entity):
    entity_name = "doc"
    Entity = s.Doc

    @endpoint()
    def on_get(self, entity, entity_id):
        set_open_status([entity])

        return Jinja(
            f"{self.entity_name}s/details.jinja2",
            {"entity_id": entity.id, "item": entity},
        )


def setup(app):
    doc = Doc()
    app.add_route("/doc/{entity_id:int}", doc)
    app.add_route("/doc/{entity_id:int}/edit", doc, suffix="edit")
    #    app.add_route("/doc/{entity_id:int}/parents", doc, suffix="parents")

    docs = Docs()
    app.add_route("/docs", docs)
    app.add_route("/docs/new", docs, suffix="new")
