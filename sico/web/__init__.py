import logging
import os
import os.path

import falcon
import bjoern
import jwt
from sqlalchemy import create_engine

from sico import config
import sico.web.docs
import sico.web.groups
import sico.web.admin
import sico.web.static
import sico.web.search
import sico.web.static_pages

from sico.db.api import Session


LOG = logging.getLogger(__name__)


class ConnectionMiddleware:
    def __init__(self, connection):
        if isinstance(connection, str):
            LOG.info("Using connection string: {}".format(connection))
            self.engine = create_engine(connection)
        else:
            self.engine = connection

    def process_request(self, req, resp):
        req.context.session = Session(bind=self.engine)

    def process_response(self, req, resp, resource, req_succeeded):
        try:
            if req_succeeded:
                req.context.session.commit()
                LOG.debug("Db changes commited.")
            else:
                req.context.session.rollback()
                LOG.debug("Exception happened. Db changes rolled back.")
        finally:
            req.context.session.close()
            LOG.debug("Db connection closed.")


class AuthenticationMiddleware:
    def process_request(self, req, resp):
        LOG.debug(f"Start authentication process: {req.relative_uri}")
        # check auth via cookie
        cookies = req.get_cookie_values("sico_auth")
        if cookies:
            LOG.debug(f"Found sico_auth cookies: {cookies}")
            token = cookies[0]
            if token:
                LOG.debug(f"Found token: {token}")
                try:
                    token = jwt.decode(token, config.secret, algorithms=["HS256"])
                    user_id = token.get("user_id")
                    if user_id:
                        req.context.user_id = user_id
                        req.context.user = user_id
                        LOG.debug(
                            "Authenticated user via cookie token. User ID: {}".format(
                                user_id
                            )
                        )
                        return
                except jwt.exceptions.InvalidSignatureError as e:
                    LOG.info(f"Found invalid token. Ignoring it: {e}")
                except jwt.exceptions.DecodeError as e:
                    LOG.info(f"Found invalid token. Ignoring it: {e}")

        req.context.user_id = None
        req.context.user = None
        LOG.debug("User not authenticated.")


class TestAuthenticationMiddleware:
    def process_request(self, req, resp):
        req.context.user_id = 1
        req.context.user = 1


def handle_error(req, resp, ex, params):
    LOG.exception("Error: {}".format(str(ex)))
    raise ex


def create_app(connection, dev_mode, test_mode=False):

    middlewares = []

    middlewares.append(ConnectionMiddleware(connection))

    if test_mode:
        middlewares.append(TestAuthenticationMiddleware())
    else:
        middlewares.append(AuthenticationMiddleware())


    app = falcon.API(middleware=middlewares)

    app.add_error_handler(RuntimeError, handle_error)

    app.req_options.auto_parse_form_urlencoded = True
    app.resp_options.secure_cookies_by_default = False

    # Resources are represented by long-lived class instances
    search = sico.web.search.Search()

    # things will handle all requests to the '/things' URL path
    app.add_route("/", search, suffix="root")
    app.add_route("/search/{status}", search)
    sico.web.static_pages.setup(app)

    sico.web.docs.setup(app)
    sico.web.groups.setup(app)
    sico.web.admin.setup(app)

    static = sico.web.static.Static()
    app.add_route("/{filename}", static)

    if dev_mode:
        static.rebuild_bundles()

    return app


def serve(connection_string, dev_mode, host="0.0.0.0", port=8000):
    LOG.info("Connection string: {}".format(connection_string))
    app = create_app(connection_string, dev_mode)

    LOG.info("Serving requests at {}:{}.".format(host, port))
    try:
        bjoern.run(app, "0.0.0.0", port)
    except:
        LOG.exception("Unhandled exception while trying to server app.")
        raise
