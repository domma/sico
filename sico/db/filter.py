from sqlalchemy.sql import or_, and_
import sqlalchemy.types as t

sql_type = {
    bool: t.Boolean,
    str: t.String,
    int: t.Integer
}


class Or:
    def __init__(self, data):
        self.children = [Filter(f) for f in data]

    def for_table(self, table):
        return or_(*[f.for_table(table) for f in self.children])


class And:
    def __init__(self, data):
        self.children = [Filter(f) for f in data]

    def for_table(self, table):
        return and_(*[f.for_table(table) for f in self.children])


class Eq:
    def __init__(self, field, value):
        self.field = field
        self.value = value

    def for_table(self, table):
        return table.data.op("->")(self.field).astext.cast(sql_type[type(self.value)]) == self.value


class Contains:
    def __init__(self, field, value):
        self.field = field
        self.value = value

    def for_table(self, table):
        return table.data.op("->")(self.field).op("?")(self.value)


def Field(data):
    field = data["field"]
    del data["field"]

    if not len(data) == 1:
        raise ValueError(
            "Field must have exactly one additional key (eq or contains) beside its name."
        )

    key, val = next(iter(data.items()))

    if key == "eq":
        return Eq(field, val)
    elif key == "contains":
        return Contains(field, val)
    else:
        raise ValueError("Field must use 'eq' or 'contains' as restriction.")


def Filter(data):
    if not type(data) == dict:
        raise ValueError("Filter expects a dict as input.")

    if "and" in data:
        if not len(data) == 1:
            raise ValueError("'and' must be the only key in a filter.")
        return And(data["and"])

    if "or" in data:
        if not len(data) == 1:
            raise ValueError("'or' must be the only key in a filter.")
        return Or(data["or"])

    if "field" in data:
        return Field(data)

    raise ValueError("Input does not match the expected format for a filter.")
