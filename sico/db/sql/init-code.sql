

create function calc_parents(the_id integer) returns integer[] as $$
        with recursive xxx as (
            select tag_id from tags_tags where child_id=the_id
            union
            select tags_tags.tag_id from tags_tags inner join xxx on tags_tags.child_id = xxx.tag_id
           ) select array(select distinct tag_id from xxx);
$$ language sql;

create function insert_trigger_calc_parents() returns trigger as $$
    begin
        update tags set parents = calc_parents(id); /* where (
            tags.id = new.child_id or new.child_id = any(tags.parents)
        );*/
        return new;
    end
$$ language plpgsql;

create function delete_trigger_calc_parents() returns trigger as $$
    begin
        update tags set parents = calc_parents(id); /*where (
            tags.id = new.child_id or new.child_id = any(tags.parents)
        );*/
        return old;
    end
$$ language plpgsql;


create trigger insert_parents
    after insert on tags_tags
    for each row
    execute procedure insert_trigger_calc_parents();

create trigger delete_parents
    after delete on tags_tags
    for each row
    execute procedure delete_trigger_calc_parents();


create function check_tag_circle() returns trigger as $$
    begin
        if exists(select * from tags where id = new.tag_id and new.child_id = any(tags.parents)) then
            raise exception 'Insert would create a circle.';
        end if;
        return new;
    end
$$ language plpgsql;

create trigger trigger_check_tag_circle
    before insert on tags_tags for each row execute procedure check_tag_circle();
