import json

from sqlalchemy import func
from sqlalchemy.sql import select

import sico.db.schema as s


def _write_table(session, entity, columns, order_columns, f):
    rows = session.query(*columns).order_by(*order_columns).all()
    for r in rows:
        json.dump(r._asdict(), f)
        f.write("\n")


def backup(session, path):
    with open(path, "w") as f:
        #   write header
        f.write("#version: 1\n")
        #   write groups
        f.write("#sico: groups\n")
        _write_table(
            session, s.Group,
            [s.Group.id, s.Group.source, s.Group.parents],
            [func.cardinality(s.Group.parents), s.Group.id], f
        )
        #   write docs
        f.write("#sico: docs\n")
        _write_table(session, s.Doc,
                [s.Doc.id, s.Doc.source],
                [s.Doc.id], f)


def _parse_items(f):
    for line in f:
        if line.startswith("#"):
            return
        yield json.loads(line)


def restore(session, path):
    group_count = session.query(s.Group).count()
    doc_count = session.query(s.Doc).count()

    if not ((group_count==0) and (doc_count==0)):
        raise RuntimeError("Database is not empty.")

    with open(path) as f:
        version = f.readline().strip()
        if not version == "#version: 1":
            raise RuntimeError(f"Found unexpected version header: {version}")

        f.readline()
        for group in _parse_items(f):
            session.add(group := s.Group.from_dict(group))

        for doc in _parse_items(f):
            session.add(s.Doc.from_dict(doc))

    session.commit()
