from sqlalchemy import func, Text, Boolean

import sico.db.schema as s

visible = (
    func.coalesce(s.Group.data.op("->")("hidden").cast(Text), "false").cast(Boolean)
    == False
)
