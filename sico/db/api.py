import logging
from collections import defaultdict

from sqlalchemy import func
from sqlalchemy.sql import select, or_
import sqlalchemy.orm.session

from sico.db.parsing import from_yaml
import sico.db.schema as s
import sico.db.backup as b

LOG = logging.getLogger(__name__)


class Api(sqlalchemy.orm.session.Session):
    def is_empty(self):
        groups = self.query([func.count(s.Group.id)]).scalar()
        if groups != 0:
            return False
        docs = self.query([func.count(s.Doc.id)]).scalar()
        if docs != 0:
            return False
        return True

    def by_id(self, entity, id):
        return self.query(entity).filter_by(id=id).one()

    def create(self, entity, source):
        e = entity(source=source, data=from_yaml(source))
        self.add(e)
        return e

    def get_user(self, login, password):
        return (
            self.query(s.User)
            .filter(s.User.login == login)
            .filter(s.User.password == password)
            .first()
        )

    def map_ids(self, entity, ids):
        items = self.query(entity).filter(entity.id.in_(tuple(ids))).all()
        items = {i.id: i for i in items}
        return [items.get(i) for i in ids]

    def get_names(self, entity):
        return (
            self.query(entity.id, entity.data["name"].label("name"))
            .filter(entity.data.has_key("name"))
            .order_by(entity.data["name"])
            .all()
        )

    def get_tag_tree(self):
        groups = self.query(s.Group).all()

        top = []

        by_id = {}

        children = defaultdict(list)

        for g in groups:
            self.expunge(g)
            g.children = []
            if not g.parents:
                top.append(g)
            by_id[g.id] = g

        for g in groups:
            for p in g.parents:
                by_id[p].children.append(g)


        return list(
            sorted(top, key=lambda g: str(g.data["name"]))
        )


def init(engine):
    s.Base.metadata.create_all(engine)

    session = Session(bind=engine)
    session.add(s.User(login="admin", password="wirbrauchen0815"))
    session.commit()



def reset(engine):
    s.Base.metadata.drop_all(engine)
    init(engine)


def backup(session, path):
    b.backup(session, path)


def restore(session, path):
    b.restore(session, path)
    session.execute("SELECT SETVAL('docs_id_seq', (SELECT MAX(id) FROM docs) + 1);")
    session.execute("SELECT SETVAL('groups_id_seq', (SELECT MAX(id) FROM groups) + 1);")


Session = sqlalchemy.orm.session.sessionmaker(class_=Api)
