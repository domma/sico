import io
import logging
import re
from itertools import chain

from ruamel.yaml import YAML

from sico.open_status import is_overlapping

LOG = logging.getLogger(__name__)

yaml = YAML(typ="safe")


valid_days = ["mo", "di", "mi", "do", "fr", "sa", "so"]


def normalize_days(text):
    text = "".join(text.lower().split())
    if text in valid_days:
        return [text]

    parts = text.split("-")
    if len(parts) < 2:
        raise ValueError(
            f"Looks like you used an invalid day specifier. You must use one of '{','.join(valid_days)}'."
        )

    if len(parts) > 2:
        raise ValueError("Looks like you specified more than two days.")

    start, end = parts
    if start not in valid_days or end not in valid_days:
        raise ValueError(
            f"Looks like you used an invalid day specifier. You must use one of '{','.join(valid_days)}'."
        )
    if start == end:
        raise ValueError("Start and end of a range must not be identical.")

    #   this will contain a full week, independent of
    #   the start day of the range
    tmp = valid_days + valid_days

    #   strip the beginning of the list to start with
    #   the given start day
    tmp = tmp[tmp.index(start) :]

    #   strip everything after the end day
    tmp = tmp[: tmp.index(end) + 1]

    return tmp


def parse_time(text):
    h, m = text.split(":")
    h = int(h)
    m = int(m)
    if not ((0 <= h < 24) and (0 <= m < 60)):
        raise ValueError(f"Invalid time: {text}")
    return h, m


def parse_time_range(text):
    start, end = text.split("-")
    return parse_time(start), parse_time(end)


def normalize_times(text):
    text = re.sub(r"\s", "", text)
    return list(sorted(parse_time_range(part) for part in text.split(",")))


def normalize_opening_hours(data):
    result = {}

    for entry in data:
        days = entry["day"]
        times = entry["time"]
        t = normalize_times(times)
        for d in normalize_days(days):
            if d in result:
                raise ValueError(
                    f"Opening hours for day '{d}' have been defined multiple times."
                )
            result[d] = t

    return result


def get_open_spans(opening_hours):
    result = {"breakfast": False, "lunch": False, "afternoon": False, "dinner": False}
    for start, end in chain(*opening_hours.values()):
        if is_overlapping((7, 0), (11, 0), start, end):
            result["breakfast"] = True
        if is_overlapping((12, 0), (14, 0), start, end):
            result["lunch"] = True
        if is_overlapping((14, 0), (17, 0), start, end):
            result["afternoon"] = True
        if is_overlapping((18, 0), (20, 0), start, end):
            result["dinner"] = True

    return result


def from_yaml(source):
    data = yaml.load(io.BytesIO(source.encode("utf-8")))
    if "open" in data:
        try:
            data["open"] = normalize_opening_hours(data["open"])
            data["open_spans"] = get_open_spans(data["open"])
        except ValueError:
            #   Currently wrong formats are just ignored, but without
            #   returning an error to the user. As soon as we have better
            #   error reporting in place, this has to be changed.
            del data["open"]
    return data
