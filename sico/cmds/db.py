import os.path

import click

from sico.cmds.main import main
from sico import config
from sico.db import api

from sqlalchemy import create_engine


@main.group()
def db():
    pass


@db.command()
def init():
    try:
        engine = create_engine(config.connection_string)
        api.init(engine)
    except Exception as e:
        click.echo(click.style(str(e), fg="red", bold=True))
        return
    click.echo(click.style("Db initialized successfully!", fg="green", bold=True))


@db.command()
def reset():
    try:
        engine = create_engine(config.connection_string)
        api.reset(engine)
    except Exception as e:
        click.echo(click.style(str(e), fg="red", bold=True))
        return
    click.echo(click.style("Db reset successfully!", fg="green", bold=True))


def get_raw_items(con, name):
    cur = con.cursor()
    cur.execute(
        """select e.id, r.source from {name}s as e
        join {name}s_versions as r on e.id = r.{name}_id and e.version = r.id""".format(
            name=name
        )
    )
    return cur.fetchall()


@db.command()
@click.argument("path")
def backup(path):
    if os.path.isfile(path):
        click.echo(click.style(f"{path} already exists.", fg="red", bold=True))
    engine = create_engine(config.connection_string)
    session = api.Session(bind=engine)
    api.backup(session, path)


@db.command()
@click.argument("path")
def restore(path):
    engine = create_engine(config.connection_string)
    session = api.Session(bind=engine)
    api.restore(session, path)
