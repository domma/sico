import os
import logging

import coloredlogs
import click
import sentry_sdk
from sentry_sdk.integrations.falcon import FalconIntegration

from sico.cmds.main import main
from sico import config
import sico.web


@main.group()
def web():
    coloredlogs.install(level=logging.DEBUG)


@web.command()
@click.option("--dev", is_flag=True)
def serve(dev):
    if not dev:
        sentry_sdk.init(
            dsn="https://ee988e4aa9144e46a141d63627d136df@o362714.ingest.sentry.io/5227243",
            integrations=[FalconIntegration()],
        )
    port = int(os.environ.get("PORT", 8080))

    sico.web.serve(config.connection_string, dev, port=port)
