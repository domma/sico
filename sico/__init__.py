import os
import configparser
import os.path

# config_path = os.path.expanduser("~/.config/sico/")
# ini_path = os.path.join(config_path, "sico.ini")
# token_path = os.path.join(config_path, "sico.auth")


class Config:
    @property
    def connection_string(self):
        return os.environ["DATABASE_URL"]

    @property
    def secret(self):
        return os.environ["SECRET"]


config = Config()
